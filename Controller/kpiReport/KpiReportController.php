<?php

namespace Terminalbd\KpiBundle\Controller\kpiReport;

use App\Entity\User;
use App\Repository\UserRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Mpdf\Mpdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\CrmBundle\Entity\Challenger;
use Terminalbd\CrmBundle\Entity\CompanyWiseFeedSale;
use Terminalbd\CrmBundle\Entity\ComplainDifferentProduct;
use Terminalbd\CrmBundle\Entity\ComplainDifferentProductDetails;
use Terminalbd\CrmBundle\Entity\DailyChickPriceDetails;
use Terminalbd\CrmBundle\Entity\NewFarmerIntroduce\FarmerIntroduceDetails;
use Terminalbd\CrmBundle\Entity\PoultryMeatEggPrice;
use Terminalbd\CrmBundle\Entity\Setting;
use Terminalbd\KpiBundle\Form\MonthRangeWiseSummeryReportFormType;
use Terminalbd\KpiBundle\Entity\AgentDocSaleCollection;
use Terminalbd\KpiBundle\Entity\AgentOrder;
use Terminalbd\KpiBundle\Entity\AgentOutstanding;
use Terminalbd\KpiBundle\Entity\DistrictOrder;
use Terminalbd\KpiBundle\Entity\EmployeeBoard;
use Terminalbd\KpiBundle\Entity\EmployeeBoardAttribute;
use Terminalbd\KpiBundle\Entity\EmployeeBoardSubAttribute;
use Terminalbd\KpiBundle\Entity\EmployeeDistrictHistory;
use Terminalbd\KpiBundle\Entity\EmployeeSetup;
use Terminalbd\KpiBundle\Entity\LocationSalesTarget;
use Terminalbd\KpiBundle\Entity\MarkChart;
use Terminalbd\KpiBundle\Form\DistrictHistorySearchFilterFormType;
use Terminalbd\KpiBundle\Form\SalesReportFilterFormType;
use Terminalbd\KpiBundle\Form\TeamMemberSummaryFilterFormType;

/**
 * Class KpiReportController
 * @package Terminalbd\KpiBundle\Controller\kpiReport
 * @Route("/kpi/report")
 * @Security("is_granted('ROLE_KPI_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_LINE_MANAGER')")
 */
class KpiReportController extends AbstractController
{
    /**
     * @Route("/team-member-summary", name="team_member_summary")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function teamMemberSummary(Request $request, UserRepository $userRepository)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');
        
        $lineManagers = $userRepository->getLineManager();
        $teamMemberSummary = [];
        $filterBy = [
            'lineManager' => array_values($lineManagers)[0],
            'startMonth' => date('F'),
            'endMonth' => date('F'),
            'year' => (int)date('Y'),
        ];
        $StartDate = @strtotime(date('F') . ' ' . (int)date('Y'));
        $StopDate = @strtotime(date('F') . ' ' . (int)date('Y'));

        $months = $this->monthRange($StartDate, $StopDate);

        $activitiesName = $this->getDoctrine()->getRepository(EmployeeBoardAttribute::class)->getActivities();
        $teamMemberSummary = $this->getDoctrine()->getRepository(EmployeeBoardAttribute::class)->getTeamMemberSummary($filterBy, $months, $this->getUser());

        $filterForm = $this->createForm(TeamMemberSummaryFilterFormType::class, null, ['user' => $this->getUser(), 'lineManagers' => $lineManagers])->remove('kpiFormat');
        $filterForm->handleRequest($request);
        if ($filterForm->isSubmitted()) {
            $filterBy = $filterForm->getData();
            $StartDate = @strtotime($filterBy['startMonth'] . ' ' . $filterBy['year']);
            $StopDate = @strtotime($filterBy['endMonth'] . ' ' . $filterBy['year']);
            $months = $this->monthRange($StartDate, $StopDate);
            $teamMemberSummary = $this->getDoctrine()->getRepository(EmployeeBoardAttribute::class)->getTeamMemberSummary($filterBy, $months, $this->getUser());

            if ($request->query->has('pdf')) {

                // Configure Dompdf according to your needs
                $pdfOptions = new Options();
                $pdfOptions->set('defaultFont', 'Arial, sans-serif');

                // Instantiate Dompdf with our options
                $dompdf = new Dompdf($pdfOptions);

                // Retrieve the HTML generated in our twig file
                $html = $this->renderView('@TerminalbdKpi/employeeboard/report/teamMemberSummary-pdf.html.twig', [
                    'filterBy' => $filterBy,
                    'teamMemberSummary' => $teamMemberSummary,
                    'activitiesName' => $activitiesName,
                ]);

                // Load HTML to Dompdf
                $dompdf->loadHtml($html);

                // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
                $dompdf->setPaper('A4', 'landscape');

                // Render the HTML as PDF
                $dompdf->render();

                // Output the generated PDF to Browser (force download)
                $fileName = $request->get('_route') . '-' . time();
                $dompdf->stream($fileName . ".pdf", [
                    "Attachment" => true
                ]);
                die();

            }
            /*            elseif ($request->query->has('excel')){
            
                            $html = $this->renderView('@TerminalbdKpi/employeeboard/report/teamMemberSummary-excel.html.twig', [
                                'filterBy' => $filterBy,
                                'teamMemberSummary' => $teamMemberSummary,
                                'activitiesName' => $activitiesName,
                            ]);
                            $fileName = $request->get('_route').'_'.time().'.xls';
                            header("Content-Type: application/vnd.ms-excel; charset=utf-8");
                            header("Content-Disposition: attachement; filename=$fileName");
                            echo $html;
                            die();
                        }*/
        }

        return $this->render('@TerminalbdKpi/employeeboard/report/teamMemberSummary.html.twig', [
            'filterBy' => $filterBy,
            'form' => $filterForm->createView(),
            'months' => $months,
            'teamMemberSummary' => $teamMemberSummary,
            'activitiesName' => $activitiesName,
        ]);

    }

    /**
     * @Route("/all-team-member-summary", name="all_team_member_summary")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function allTeamMemberSummary(Request $request, UserRepository $userRepository)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $lineManagers = $userRepository->getLineManager();

        $filterBy = [
            "kpiFormat" => null,
            "startMonth" => date('F'),
            "endMonth" => date('F'),
            "year" => (int)date('Y'),
        ];
        $StartDate = @strtotime(date('F') . ' ' . (int)date('Y'));
        $StopDate = @strtotime(date('F') . ' ' . (int)date('Y'));

        $months = $this->monthRange($StartDate, $StopDate);

        $teamMemberSummary = $this->getDoctrine()->getRepository(EmployeeBoardAttribute::class)->getTeamMemberSummary($filterBy, $months, $this->getUser());
        $activitiesName = $this->getDoctrine()->getRepository(EmployeeBoardAttribute::class)->getActivities();

        $filterForm = $this->createForm(TeamMemberSummaryFilterFormType::class, null, ['user' => $this->getUser(), 'lineManagers' => $lineManagers])->remove('lineManager')->remove('employee');
        $filterForm->handleRequest($request);
        if ($filterForm->isSubmitted()) {
            $filterBy = $filterForm->getData();

            $filterBy['kpiFormat'] = $filterBy['kpiFormat'] ? $filterBy['kpiFormat']->getId() : null;
            $StartDate = @strtotime($filterBy['startMonth'] . ' ' . $filterBy['year']);
            $StopDate = @strtotime($filterBy['endMonth'] . ' ' . $filterBy['year']);

            $months = $this->monthRange($StartDate, $StopDate);
            $teamMemberSummary = $this->getDoctrine()->getRepository(EmployeeBoardAttribute::class)->getTeamMemberSummary($filterBy, $months, $this->getUser());
            if ($request->query->has('pdf')) {

                // Configure Dompdf according to your needs
                $pdfOptions = new Options();
                $pdfOptions->set('defaultFont', 'Arial, sans-serif');

                // Instantiate Dompdf with our options
                $dompdf = new Dompdf($pdfOptions);

                // Retrieve the HTML generated in our twig file
                $html = $this->renderView('@TerminalbdKpi/employeeboard/report/allTeamMemberSummary-pdf.html.twig', [
                    'filterBy' => $filterBy,
                    'teamMemberSummary' => $teamMemberSummary,
                    'activitiesName' => $activitiesName,
                ]);

                // Load HTML to Dompdf
                $dompdf->loadHtml($html);

                // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
                $dompdf->setPaper('A4', 'landscape');

                // Render the HTML as PDF
                $dompdf->render();

                // Output the generated PDF to Browser (force download)
                $fileName = $request->get('_route') . '-' . time();
                $dompdf->stream($fileName . ".pdf", [
                    "Attachment" => true
                ]);
                die();
            }
            /*            elseif ($request->query->has('excel')){
                            
                            $html = $this->renderView('@TerminalbdKpi/employeeboard/report/allTeamMemberSummary-excel.html.twig', [
                                'filterBy' => $filterBy,
                                'teamMemberSummary' => $teamMemberSummary,
                                'activitiesName' => $activitiesName,
                            ]);
                            $fileName = $request->get('_route').'_'.time().'.xls';
                            header("Content-Type: application/vnd.ms-excel; charset=utf-8");
                            header("Content-Disposition: attachment; filename=$fileName");
                            echo $html;
                            die();
            
                        }*/
        }


        return $this->render('@TerminalbdKpi/employeeboard/report/allTeamMemberSummary.html.twig', [
            'filterBy' => $filterBy,
            'form' => $filterForm->createView(),
            'teamMemberSummary' => $teamMemberSummary,
            'months' => $months,
            'activitiesName' => $activitiesName,
        ]);
    }

    /**
     * @param Request $request
     * @Route("/district-history", name="district_history")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function districtHistory(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');
        $filterBy = [
            'month' => date('F'),
            'year' => date('Y'),
            'user' => $this->getUser(),
            'employee' => null,
        ];

//        $form = $this->createForm(DistrictHistorySearchFilterFormType::class, null, ['user' => $this->getUser()]);
        $form = $this->createForm(DistrictHistorySearchFilterFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $filterBy = $form->getData();
            $filterBy['user'] = $this->getUser();

            $data = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->getDistrictHistory($filterBy);
            if ($request->query->has('pdf')) {

                // Configure Dompdf according to your needs
                $pdfOptions = new Options();
                $pdfOptions->set('defaultFont', 'Arial, sans-serif');

                // Instantiate Dompdf with our options
                $dompdf = new Dompdf($pdfOptions);

                // Retrieve the HTML generated in our twig file
                $html = $this->renderView('@TerminalbdKpi/employeeboard/report/districtHistory-pdf.html.twig', [
                    'data' => $data,
                    'filterBy' => $filterBy
                ]);

                // Load HTML to Dompdf
                $dompdf->loadHtml($html);

                // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
                $dompdf->setPaper('A4', 'portrait');

                // Render the HTML as PDF
                $dompdf->render();

                // Output the generated PDF to Browser (force download)
                $fileName = $request->get('_route') . '-' . time();
                $dompdf->stream($fileName . ".pdf", [
                    "Attachment" => true
                ]);
                die();
            }
        }
        $data = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->getDistrictHistory($filterBy);
        return $this->render('@TerminalbdKpi/employeeboard/report/districtHistory.html.twig', [
            'form' => $form->createView(),
            'data' => $data
        ]);
    }

    /**
     * @Route("/monthly-generated-kpi-status", name="monthly_generated_kpi_status")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function monthlyGeneratedKpiStatus(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');
        
        $user = $this->getUser();

        $employees = [];
        $lineManagers = [];

        $selectedYear = $request->query->get('year');
        $selectedLineManager = $request->query->get('lineManager');

        if (!$selectedYear) {
            $selectedYear = date('Y');
        }

        if (in_array('ROLE_KPI_ADMIN', $user->getRoles())) {

            $lineManagers = $this->getDoctrine()->getRepository(User::class)->getLineManagers();
            $lineManagersId = [];
            if ($selectedLineManager) {
                $lineManagersId[] = $selectedLineManager;
            } else {
                foreach ($lineManagers as $lineManager) {
                    $lineManagersId[] = $lineManager['userId'];
                }
            }
            $employees = $this->getDoctrine()->getRepository(User::class)->getLineManagerTeamMember($lineManagersId, []);
        }
        
        $kpiRecords = $this->getDoctrine()->getRepository(EmployeeBoard::class)->getMonthlyStatus($selectedYear, $selectedLineManager, $user);

        return $this->render('@TerminalbdKpi/employeeboard/report/monthlyStatus.html.twig', [
            'kpiRecords' => $kpiRecords,
            'year' => $selectedYear,
            'lineManagers' => $lineManagers,
            'employees' => $employees,
            'selectedLineManager' => $selectedLineManager,
        ]);
    }

    /**
     * @Route("/{mode}/sales-percentage", name="sales_percentage")
     * @param Request $request
     * @param \Symfony\Component\HttpFoundation\Response
     */
    public function salesPercentage(Request $request, $mode)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $startDate = @strtotime(date('F') . ' ' . (int)date('Y'));
        $endDate = @strtotime(date('F') . ' ' . (int)date('Y'));

        $months = $this->monthRange($startDate, $endDate);
        $data = [];
        $filterBy = [
            'loggedUser' => $this->getUser(),
            'employee' => null,
            'startMonth' => date('F'),
            'endMonth' => date('F'),
            'year' => (int)date('Y'),
            'salesMode' => $mode,
        ];

        $filterForm = $this->createForm(SalesReportFilterFormType::class, null, ['user' => $this->getUser()]);
        $filterForm->handleRequest($request);

        if ($filterForm->isSubmitted()){
            $startDate = @strtotime($filterForm->get('startMonth')->getData() . ' ' . $filterForm->get('year')->getData());
            $endDate = @strtotime($filterForm->get('endMonth')->getData() . ' ' . $filterForm->get('year')->getData());
            $months = $this->monthRange($startDate, $endDate);
//dd($months);
            $filterBy['employee'] = $filterForm->get('employee')->getData();
            $filterBy['year'] = (int)$filterForm->get('year')->getData();
            $filterBy['months'] = $months;
            $filterBy['startMonth'] = $filterForm->get('startMonth')->getData();
            $filterBy['endMonth'] = $filterForm->get('endMonth')->getData();

            if($mode=='growth'){
                $data = $this->getDoctrine()->getRepository(EmployeeBoard::class)->salesPercentageReport($filterBy, true);
            }else{
                $data = $this->getDoctrine()->getRepository(EmployeeBoard::class)->salesPercentageReport($filterBy);
            }
        }
        if ($request->query->get('pdf')){

            // Configure Dompdf according to your needs
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial, sans-serif');
            $pdfOptions->set('isHtml5ParserEnabled', true);
            $pdfOptions->set('isRemoteEnabled', true);
            $pdfOptions->set('debugCss', true); // Useful for debugging CSS issues
            $pdfOptions->set('isPhpEnabled', true);

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);
            $html='';
            $fileName='';
            if($mode=='growth'){
                $html= $this->renderView('@TerminalbdKpi/employeeboard/report/salesGrowthPercentage-pdf.html.twig', [
                    'filterBy' => $filterBy,
                    'data' => $data,
                    'mode' => $mode,
                ]);
                $fileName = 'growth_'.$request->get('_route') . '-' . time();
            }else{
                // Retrieve the HTML generated in our twig file
                $html = $this->renderView('@TerminalbdKpi/employeeboard/report/salesPercentage-pdf.html.twig', [
                    'data' => $data,
                    'filterBy' => $filterBy,
                    'mode' => $mode,

                ]);
                $fileName = $request->get('_route') . '-' . time();
            }


            // Load HTML to Dompdf
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
            $dompdf->setPaper('A3', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser (force download)

            $dompdf->stream($fileName . ".pdf", [
                "Attachment" => true
            ]);
            die();
        }

        if($mode=='growth'){
            return $this->render('@TerminalbdKpi/employeeboard/report/salesGrowthPercentage.html.twig', [
                'filterBy' => $filterBy,
                'form' => $filterForm->createView(),
                'data' => $data,
                'mode' => $mode,
            ]);
        }

        return $this->render('@TerminalbdKpi/employeeboard/report/salesPercentage.html.twig', [
            'filterBy' => $filterBy,
            'form' => $filterForm->createView(),
            'data' => $data,
            'mode' => $mode,
        ]);
    }


    /**
     * @Route("/{mode}/marks-percentage", name="marks_percentage")
     * @param Request $request
     * @param \Symfony\Component\HttpFoundation\Response
     */
    public function marksPercentage(Request $request, $mode)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $startDate = @strtotime(date('F') . ' ' . (int)date('Y'));
        $endDate = @strtotime(date('F') . ' ' . (int)date('Y'));

        $months = $this->monthRange($startDate, $endDate);

        $filterBy = [
            'loggedUser' => $this->getUser(),
            'employee' => null,
            'months' => $months,
            'year' => (int)date('Y'),
            'salesMode' => $mode,
        ];

        $filterForm = $this->createForm(SalesReportFilterFormType::class, null, ['user' => $this->getUser()]);
        $filterForm->handleRequest($request);

        if ($filterForm->isSubmitted()){
            $startDate = @strtotime($filterForm->get('startMonth')->getData() . ' ' . $filterForm->get('year')->getData());
            $endDate = @strtotime($filterForm->get('endMonth')->getData() . ' ' . $filterForm->get('year')->getData());
            $months = $this->monthRange($startDate, $endDate);

            $filterBy['employee'] = $filterForm->get('employee')->getData();
            $filterBy['year'] = (int)$filterForm->get('year')->getData();
            $filterBy['months'] = $this->monthRange($startDate, $endDate);

        }

        $data = $this->getDoctrine()->getRepository(EmployeeBoardSubAttribute::class)->getSalesTargetAchievementMarksReport($filterBy);

        if ($request->query->get('pdf')){

            // Configure Dompdf according to your needs
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial, sans-serif');

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);

            // Retrieve the HTML generated in our twig file
            $html = $this->renderView('@TerminalbdKpi/employeeboard/report/salesMarksPercentage-pdf.html.twig', [
                'data' => $data,
                'filterBy' => $filterBy,
                'mode' => $mode,

            ]);

            // Load HTML to Dompdf
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
            $dompdf->setPaper('A3', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser (force download)
            $fileName = $request->get('_route') . '-' . time();
            $dompdf->stream($fileName . ".pdf", [
                "Attachment" => true
            ]);
            die();
        }

        return $this->render('@TerminalbdKpi/employeeboard/report/salesMarksPercentage.html.twig', [
            'filterBy' => $filterBy,
            'form' => $filterForm->createView(),
            'data' => $data,
            'mode' => $mode,
        ]);
    }

    /**
     * @Route("/sales-growth-marks-summary", name="sales_growth_marks_summary")
     * @param Request $request
     * @param \Symfony\Component\HttpFoundation\Response
     */
    public function salesGrowthMarksSummary(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $startDate = @strtotime(date('F') . ' ' . (int)date('Y'));
        $endDate = @strtotime(date('F') . ' ' . (int)date('Y'));

        $months = $this->monthRange($startDate, $endDate);

        $filterBy = [
            'loggedUser' => $this->getUser(),
            'employee' => null,
            'months' => $months,
            'year' => (int)date('Y'),
            'slugs' => ['sales', 'sales-growth-always-consider-among-same-period'],
        ];

        $filterForm = $this->createForm(SalesReportFilterFormType::class, null, ['user' => $this->getUser()]);
        $filterForm->handleRequest($request);

        if ($filterForm->isSubmitted()){
            $startDate = @strtotime($filterForm->get('startMonth')->getData() . ' ' . $filterForm->get('year')->getData());
            $endDate = @strtotime($filterForm->get('endMonth')->getData() . ' ' . $filterForm->get('year')->getData());
            $months = $this->monthRange($startDate, $endDate);

            $filterBy['employee'] = $filterForm->get('employee')->getData();
            $filterBy['year'] = (int)$filterForm->get('year')->getData();
            $filterBy['months'] = $this->monthRange($startDate, $endDate);

        }


        $data = $this->getDoctrine()->getRepository(EmployeeBoardAttribute::class)->getSalesGrowthReport($filterBy);

        if ($request->query->get('pdf')){

            // Configure Dompdf according to your needs
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial, sans-serif');

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);

            // Retrieve the HTML generated in our twig file
            $html = $this->renderView('@TerminalbdKpi/employeeboard/report/salesGrowthMarksSummary-pdf.html.twig', [
                'data' => $data,
                'filterBy' => $filterBy,

            ]);

            // Load HTML to Dompdf
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
            $dompdf->setPaper('A3', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser (force download)
            $fileName = $request->get('_route') . '-' . time();
            $dompdf->stream($fileName . ".pdf", [
                "Attachment" => true
            ]);
            die();
        }

        return $this->render('@TerminalbdKpi/employeeboard/report/salesGrowthMarksSummary.html.twig', [
            'filterBy' => $filterBy,
            'form' => $filterForm->createView(),
            'data' => $data,
        ]);
    }
    
    /**
     * Gets list of months between two dates
     * @param int $start Unix timestamp
     * @param int $end Unix timestamp
     * @return array
     */
    private function monthRange($start, $end)
    {

        $current = $start;
        $data = [];
        while ($current <= $end) {

//            $next = @date('Y-M-01', $current) . "+1 month";
            $next = @date('Y-M-01', $current);
            $current = @strtotime($next);

            $data[] = date('F', $current);

            $next = @date('Y-M-01', $current) . "+1 month";
            $current = @strtotime($next);
        }
        return $data;
    }


    /**
     * @Route("kpi/kpi-report", name="kpi_report")
     */
    public function generateKpiReport()
    {
        return false;
        /*        $marksDistributions = [];
                $user = $this->getUser();
                $employee = $this->getDoctrine()->getRepository(EmployeeSetup::class)->getEmployeeList($user);
                $products = $this->getDoctrine()->getRepository(LocationSalesTarget::class)->getProductTarget();
                $attributesAndMarks = $this->getDoctrine()->getRepository(MarkChart::class)->getAttributes();
        
                dd($products);
                foreach ($products as $key => $product){
                    dd($product);
                    $agentOrder = $this->getDoctrine()->getRepository(AgentOrder::class)->getCompletedAmount($key);
        
                }*/

        /*        foreach ($attributesAndMarks as $attributesAndMark) {
        //            echo $attributesAndMark['attributesName'] . '<br>';
                    $marksDistributions[$attributesAndMark['attributesName']] = $this->getDoctrine()->getRepository(MarkChart::class)->getMarkDistribution($attributesAndMark['attributesName']);
                }
                foreach ($marksDistributions as $marksDistribution) {
                    $attributesAndMarks['markDistribution'][] = $marksDistribution;
                }
                $marksDistributions = $this->getDoctrine()->getRepository(MarkChart::class)->getMarkDistribution('Poultry');
        //        dd($attributesAndMarks);*/

        /*        return $this->render('@TerminalbdKpi/kpiReport/bank-satement.html.twig',[
                    'attributesAndMarks'  => $attributesAndMarks
                ]);*/
    }

    /**
     * @Route("/kpi-monthly-summery-report", name="kpi_monthly_summery_report")
     * @param Request $request
     * @param \Symfony\Component\HttpFoundation\Response
     */
    public function kpiMonthlySummery(Request $request, $mode='feed')
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $startDate = @strtotime(date('F') . ' ' . (int)date('Y'));
        $endDate = @strtotime(date('F') . ' ' . (int)date('Y'));

        $months = $this->monthRange($startDate, $endDate);
        $data=[];

        $filterBy = [
            'loggedUser' => $this->getUser(),
            'employee' => null,
            'startMonth' => date('F'),
            'endMonth' => date('F'),
            'year' => (int)date('Y'),
            'salesMode' => $mode,
            'reportType' => null
        ];

        $filterForm = $this->createForm(SalesReportFilterFormType::class, null, ['user' => $this->getUser()]);
        //add field
        $filterForm->add('reportType', ChoiceType::class, [
            'choices' => [
                'Sales & Outstanding'=> 'sales_outstanding',
                'Competitors & Activities'=>'competitor_activities',
            ],
            'required' => false,
            'placeholder' => 'Select Type',
        ]);
        $filterForm->handleRequest($request);

        if ($filterForm->isSubmitted()){

            $startDate = @strtotime($filterForm->get('startMonth')->getData() . ' ' . $filterForm->get('year')->getData());
            $endDate = @strtotime($filterForm->get('endMonth')->getData() . ' ' . $filterForm->get('year')->getData());
            $months = $this->monthRange($startDate, $endDate);

//            dd($months);

            $filterBy['employee'] = $filterForm->get('employee')->getData();
            $filterBy['year'] = (int)$filterForm->get('year')->getData();
            $filterBy['months'] = $months;
            $filterBy['startMonth'] = $filterForm->get('startMonth')->getData();
            $filterBy['endMonth'] = $filterForm->get('endMonth')->getData();
            $filterBy['reportType'] = $filterForm->get('reportType')->getData();

//dd($teamMembers);
            $outstandingAndDocSales=[];
            $teamMemberOutstandingAndDocSales=[];
            if(sizeof($months)>0 && $filterBy['employee']!=''){
                $teamMembers = $this->getDoctrine()->getRepository(User::class)->getPermanentTeamMemberByLineManager($filterForm->get('employee')->getData());
//                dd($teamMembers);
                foreach ($months as $month) {
                    $districtHistory = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->findOneBy(['employee' => $filterForm->get('employee')->getData(), 'month' => $month, 'year' => $filterBy['year']]);
                    $districts = $districtHistory ? $districtHistory->getDistrict() : '';
                    $districtsId = $districts ? array_keys(json_decode($districts, true)) : [];
                    $outstandingAndDocSales['outstanding'][$month] = $this->getDoctrine()->getRepository(AgentOutstanding::class)->getMonthYearDistrictsWiseOutstanding($districtsId, $filterBy['year'], $month);
                    $outstandingAndDocSales['docSales'][$month] = $this->getDoctrine()->getRepository(AgentDocSaleCollection::class)->getMonthYearDistrictsWiseDocSales($districtsId, $filterBy['year'], $month);

                    $outstandingAndDocSales['feedSalesDataPreviousYear'][] = $this->getDoctrine()->getRepository(DistrictOrder::class)->getSalesTargetAndAchievementForMonthlyReport($districtsId, $filterBy, $month, true);
                    $outstandingAndDocSales['feedSalesData'][] = $this->getDoctrine()->getRepository(DistrictOrder::class)->getSalesTargetAndAchievementForMonthlyReport($districtsId, $filterBy, $month);
                }


                if(sizeof($teamMembers)>0){
                    foreach ($teamMembers as $teamMember) {
                        foreach ($months as $month) {
                            $teamMemberDistrictHistory = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->findOneBy(['employee' => $teamMember['id'], 'month' => $month, 'year' => $filterBy['year']]);
                            $teamMemberDistricts = $teamMemberDistrictHistory ? $teamMemberDistrictHistory->getDistrict() : '';
                            $teamMemberDistrictsId = $teamMemberDistricts ? array_keys(json_decode($teamMemberDistricts, true)) : [];

                            $teamMemberOutstandingAndDocSales['feedSalesDataPreviousYear'][$teamMember['id']][] = $this->getDoctrine()->getRepository(DistrictOrder::class)->getSalesTargetAndAchievementForMonthlyReport($teamMemberDistrictsId, $filterBy, $month, true);
                            $teamMemberOutstandingAndDocSales['feedSalesData'][$teamMember['id']][] = $this->getDoctrine()->getRepository(DistrictOrder::class)->getSalesTargetAndAchievementForMonthlyReport($teamMemberDistrictsId, $filterBy, $month);
                        }
                    }
                }

            }


            $proviousYearFeedSales = [];
            if(isset($outstandingAndDocSales['feedSalesDataPreviousYear']) && sizeof($outstandingAndDocSales['feedSalesDataPreviousYear']) > 0){
                foreach ($outstandingAndDocSales['feedSalesDataPreviousYear'] as $feedSalesData) {
                    foreach ($feedSalesData as $breedName =>  $breedWiseData) {
                        foreach ($breedWiseData as $monthName =>  $value) {
//                        dd($value);
                            $proviousYearFeedSales[$breedName][$monthName]=$value;
                        }
                    }
                }
            }

            $currentYearFeedSales = [];
            if(isset($outstandingAndDocSales['feedSalesData']) && sizeof($outstandingAndDocSales['feedSalesData']) > 0){
                foreach ($outstandingAndDocSales['feedSalesData'] as $feedSalesData) {
                    foreach ($feedSalesData as $breedName =>  $breedWiseData) {
                        foreach ($breedWiseData as $monthName =>  $value) {
                            $currentYearFeedSales[$breedName][$monthName]=$value;
                        }
                    }
                }
            }

            $proviousYearTeamMembersFeedSales = [];
            if(isset($teamMemberOutstandingAndDocSales['feedSalesDataPreviousYear']) && sizeof($teamMemberOutstandingAndDocSales['feedSalesDataPreviousYear']) > 0){
                foreach ($teamMemberOutstandingAndDocSales['feedSalesDataPreviousYear'] as $employeeId => $employeeWiseData) {
                    foreach ($employeeWiseData as $feedSalesData) {
                        foreach ($feedSalesData as $breedName =>  $breedWiseData) {
                            foreach ($breedWiseData as $monthName =>  $value) {
                                $proviousYearTeamMembersFeedSales['data'][$breedName][$employeeId][]=$value;
                            }
                        }
                    }
                }
            }

            $currentYearTeamMembersFeedSales = [];
            if(isset($teamMemberOutstandingAndDocSales['feedSalesData']) && sizeof($teamMemberOutstandingAndDocSales['feedSalesData']) > 0){
                foreach ($teamMemberOutstandingAndDocSales['feedSalesData'] as $employeeId => $employeeWiseData) {
                    foreach ($employeeWiseData as $feedSalesData) {
                        foreach ($feedSalesData as $breedName =>  $breedWiseData) {
                            foreach ($breedWiseData as $monthName =>  $value) {
                                $currentYearTeamMembersFeedSales['employeeInfo'][$employeeId] = $teamMembers[$employeeId];
                                $currentYearTeamMembersFeedSales['data'][$breedName][$employeeId][]=$value;
                            }
                        }
                    }
                }
            }

//            dd($currentYearTeamMembersFeedSales['data']);



//            dd($currentYearFeedSales);

            $data['avgDocPrice'] = $this->getDoctrine()->getRepository(DailyChickPriceDetails::class)->getEmployeeMonthCompanyWiseAvgDocPriceMonthly($filterBy);

            $data ['meatAndEggPrice'] = $this->getDoctrine()->getRepository(PoultryMeatEggPrice::class)->getMeatEggPriceEmployeeAndDateRangeWiseReport($filterBy);


//            dd($data['meatAndEggPrice']);
            
            
//            $data['previousYearSalesData'] = $this->getDoctrine()->getRepository(EmployeeBoardSubAttribute::class)->getSalesTargetAchievementSummeryReport($filterBy, true);
//            $data['currentYearSalesData'] = $this->getDoctrine()->getRepository(EmployeeBoardSubAttribute::class)->getSalesTargetAchievementSummeryReport($filterBy);
            $data['previousYearSalesData'] = $proviousYearFeedSales;
            $data['currentYearSalesData'] = $currentYearFeedSales;

            $data['outstandingAndDocSales'] = $outstandingAndDocSales;

//            $data['previousYearTeamMemberSalesData'] = $this->getDoctrine()->getRepository(EmployeeBoardSubAttribute::class)->getTeamMemberSalesTargetAchievementSummeryReport($filterBy, true);
            $data['previousYearTeamMemberSalesData'] = $proviousYearTeamMembersFeedSales;
//            $data['currentYearTeamMemberSalesData'] = $this->getDoctrine()->getRepository(EmployeeBoardSubAttribute::class)->getTeamMemberSalesTargetAchievementSummeryReport($filterBy);
            $data['currentYearTeamMemberSalesData'] = $currentYearTeamMembersFeedSales;

//            dd( $proviousYearTeamMembersFeedSales, $currentYearTeamMembersFeedSales);


            $data['companyWiseFeedSales'] = $this->getDoctrine()->getRepository(CompanyWiseFeedSale::class)->getCompanyWiseFeedSaleForMonthlyReport( $filterBy);
            $data['companyWiseFeedSales']['species'] = $this->getDoctrine()->getRepository(Setting::class)->getAllProductTypeForMonthlyReport();

            $data['docComplain'] = $this->getDoctrine()->getRepository(ComplainDifferentProductDetails::class)->getComplainReportByEmployeeForMonthlyReport($filterBy, 'COMPLAIN_DOC');

            $data['feedComplain'] = $this->getDoctrine()->getRepository(ComplainDifferentProductDetails::class)->getComplainReportByEmployeeForMonthlyReport($filterBy, 'COMPLAIN_FEED');

            $report = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(['settingType'=>'FARMER_REPORT','slug'=>'farmer-introduce-report-cattle', 'status'=>1]);

            $data['cattleFarmIntroduce'] = $this->getDoctrine()->getRepository(FarmerIntroduceDetails::class)->getFarmerIntroduceReportByEmployeeDateForKpiMonthlyReport($report, $filterBy);

            $data['challengesProblem'] = $this->getDoctrine()->getRepository(Challenger::class)->getChallengerByEmployeeForKpiMonthlyReport('challenges-problem', $filterBy);
            $data['challengesIdea'] = $this->getDoctrine()->getRepository(Challenger::class)->getChallengerByEmployeeForKpiMonthlyReport('challenges-idea', $filterBy);
            $data['challengesCompetitorActivity'] = $this->getDoctrine()->getRepository(Challenger::class)->getChallengerByEmployeeForKpiMonthlyReport('competitors-activity', $filterBy);




//            dd($data['challengesProblem']);
        }
        
        if ($request->query->get('pdf')){


            $mpdf = new Mpdf([
                'tempDir' => __DIR__ . '/../../../../../public/temp',
                'default_font_size' => 14,
                'default_font' => 'SolaimanLipi',
//    'autoScriptToLang' => true,
                'autoLangToFont' => true,
//                'autoVietnamese' => true,
                'mode' => 'utf-8',
            ]);
            $html = $this->renderView('@TerminalbdKpi/employeeboard/report/monthlySummeryReport-pdf.html.twig', [
                'filterBy' => $filterBy,
                'data' => $data,
                'mode' => $mode,
                'months' => $months,

            ]);

            $mpdf->WriteHTML($html);
            // render the pdf
            $mpdf->Output(time().'_kpi_monthly_summery_report.pdf', 'D');
            die;
        }

        return $this->render('@TerminalbdKpi/employeeboard/report/monthlySummeryReport.html.twig', [
            'filterBy' => $filterBy,
            'form' => $filterForm->createView(),
            'data' => $data,
            'mode' => $mode,
            'months' => $months,
        ]);
    }

    /**
     * @Route("/kpi-month-range-wise-summery-report", name="kpi_month_range_wise_summery_report")
     * @param Request $request
     * @param \Symfony\Component\HttpFoundation\Response
     */
    public function kpiMonthRangeWiseSummery(Request $request, $mode='feed')
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        $startDate = @strtotime(date('F') . ' ' . (int)date('Y'));
        $endDate = @strtotime(date('F') . ' ' . (int)date('Y'));

        $months = $this->monthRange($startDate, $endDate);
        $data=[];

        $filterBy = [
            'loggedUser' => $this->getUser(),
            'employee' => null,
            'startMonth' => date('F'),
            'endMonth' => date('F'),
            'year' => (int)date('Y'),
            'salesMode' => $mode,
        ];
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $filterForm = $this->createForm(MonthRangeWiseSummeryReportFormType::class, null, ['userRepo'=>$userRepo, 'method' => 'GET']);


//        $filterForm = $this->createForm(SalesReportFilterFormType::class, null, ['user' => $this->getUser()]);

        $filterForm->handleRequest($request);

        $employees=[];
        $fromDate = date('Y-m');
        $toDate = date('Y-m');
        $months=[];
        $filterBy = [];

        if ($filterForm->isSubmitted()){

            $filterBy = $filterForm->getData();
            $lineManager = isset($filterBy['lineManager']) && $filterBy['lineManager'] != '' ? $filterBy['lineManager'] : null;
            $employee = isset($filterBy['employee']) && $filterBy['employee'] != '' ? $filterBy['employee'] : null;
            $fromDate = isset($filterBy['fromDate']) ? $filterBy['fromDate']->format("Y-m") : date('Y-m');
            $toDate = isset($filterBy['toDate']) ? $filterBy['toDate']->format("Y-m") : date('Y-m');
            $roleSplitArray = [];
            $userRoles = [];
            $employeeArray=[];

            foreach ($this->getUser()->getRoles() as $role) {
                $roleSplitArray = array_merge(explode('_', $role), $roleSplitArray);
            }

            if (in_array('ADMIN', $roleSplitArray)) {
                if (in_array('ROLE_CRM_POULTRY_ADMIN', $this->getUser()->getRoles())) {
                    array_push($userRoles, 'ROLE_CRM_POULTRY_USER');
                }
                if (in_array('ROLE_CRM_CATTLE_ADMIN', $this->getUser()->getRoles())) {
                    array_push($userRoles, 'ROLE_CRM_CATTLE_USER');
                }
                if (in_array('ROLE_CRM_AQUA_ADMIN', $this->getUser()->getRoles())) {
                    array_push($userRoles, 'ROLE_CRM_AQUA_USER');
                }
                if (in_array('ROLE_CRM_SALES_MARKETING_ADMIN', $this->getUser()->getRoles())) {
                    array_push($userRoles, 'ROLE_CRM_SALES_MARKETING_USER');
                }
                $employeeArray = $this->getDoctrine()->getRepository(User::class)->getRoleWiseEmployees($userRoles);
            }elseif (!in_array('ADMIN', $roleSplitArray) && in_array('ROLE_LINE_MANAGER', $this->getUser()->getRoles())){
                $employeeArray = $this->getDoctrine()->getRepository(User::class)->getEmployeesByEmployeeIds($this->getUser());
            }
            if($lineManager){
                $employeeArray = $this->getDoctrine()->getRepository(User::class)->getEmployeesByEmployeeIds($lineManager);
            }
            if($employee){
                $employeeArray = $this->getDoctrine()->getRepository(User::class)->getEmployeesByEmployeeIds($employee->getLineManager(), $employee->getId());
            }

            $uniqueEmployees = [];
            if(isset($employeeArray['employee']) && sizeof($employeeArray['employee'])>0){
                $uniqueEmployees = $this->unique_array($employeeArray['employee'], 'id');
            }
            if(sizeof($uniqueEmployees)>0){
                foreach ($uniqueEmployees as $employee) {
                    if(isset($employee['serviceModeSlug']) && $employee['serviceModeSlug']=='sales-marketing'){
                        $employees[$employee['lineManagerId']][] = $employee;
                    }
//                    $employees[$employee['lineManagerId']][] = $employee;
                }
            }

            $startDate = date('Y-m-01', strtotime($fromDate));
            $endDate = date('Y-m-t', strtotime($toDate));
            $months = $this->getMonthsInRange($startDate, $endDate);

            $employeeIds = array_column($uniqueEmployees, 'id');

            $data['challengesProblem'] = $this->getDoctrine()->getRepository(Challenger::class)->getChallengerByMonthRangeAndEmployeeIdsForKpiMonthlyReport('challenges-problem', $startDate, $endDate, $employeeIds);
            $data['challengesIdea'] = $this->getDoctrine()->getRepository(Challenger::class)->getChallengerByMonthRangeAndEmployeeIdsForKpiMonthlyReport('challenges-idea', $startDate, $endDate, $employeeIds);
            $data['challengesCompetitorActivity'] = $this->getDoctrine()->getRepository(Challenger::class)->getChallengerByMonthRangeAndEmployeeIdsForKpiMonthlyReport('competitors-activity', $startDate, $endDate, $employeeIds);

            $data['docPrice'] = $this->getDoctrine()->getRepository(DailyChickPriceDetails::class)->getDocPriceByMonthRangeWise($startDate, $endDate, $employeeIds);

            $data['docComplain'] = $this->getDoctrine()->getRepository(ComplainDifferentProductDetails::class)->getComplainReportByMonthRangeAndEmployeeIds($startDate, $endDate, $employeeIds, 'COMPLAIN_DOC');

            $data['feedComplain'] = $this->getDoctrine()->getRepository(ComplainDifferentProductDetails::class)->getComplainReportByMonthRangeAndEmployeeIds($startDate, $endDate, $employeeIds, 'COMPLAIN_FEED');
            $data ['meatAndEggPrice'] = $this->getDoctrine()->getRepository(PoultryMeatEggPrice::class)->getMeatEggPriceByEmployeeIdsAndDateRangeWiseReport($startDate, $endDate, $employeeIds);

            $data['companyWiseFeedSales'] = $this->getDoctrine()->getRepository(CompanyWiseFeedSale::class)->getCompanyWiseFeedSaleByEmployeeIdsMonthRange( $startDate, $endDate, $employeeIds);

        }

        $meatAndEggTypes = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('settingType'=>'MEAT_EGG_TYPE', 'status' => 1));
        $meatTypes = $meatAndEggTypes ? array_filter($meatAndEggTypes, function($setting){
            return $setting->getSlug() == 'broiler-meat' || $setting->getSlug() == 'sonali-meat';
        }) : [];

        $eggTypes = $meatAndEggTypes ? array_filter($meatAndEggTypes, function($setting){
            return $setting->getSlug() == 'brown-layer-egg' || $setting->getSlug() == 'white-layer-egg';
        }) : [];

        $chickTypes = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('settingType'=>'CHICK_TYPE', 'status' => 1));
        $breedTypes = $this->getDoctrine()->getRepository(ComplainDifferentProduct::class)->getComplainBreedAndFeedByType('COMPLAIN_DOC');
        $feedTypes = $this->getDoctrine()->getRepository(ComplainDifferentProduct::class)->getComplainBreedAndFeedByType('COMPLAIN_FEED');

        $breedNameArray = [
            'poultry-layer-chicks'=>'Doc Production',
            'poultry'=>'Poultry feed Sales',
//            'poultry-boiler-chicks',
            'cattle'=>'Cattle feed Sales',
            'fish'=>'Fish feed Sales',
        ];
        $productsName = [];
        foreach ($breedNameArray as $breed_name=>$breedTitle) {
            $breedParam = $breed_name;
            $breedExplode = explode('-', $breed_name);
            $breed_name = isset($breedExplode[0]) ? $breedExplode[0] : '';
            $breedType = isset($breedExplode[1]) ? $breedExplode[1] : null;

            $breedNameObj = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('status'=>1, 'settingType'=>'BREED_NAME','slug'=>$breed_name.'-breed'));

            $farmTypesByParent = $this->getDoctrine()->getRepository(Setting::class)->findBy(array('status'=>1,'settingType'=>'FARM_TYPE','parent'=>$breedNameObj));

            $farmTypeId = [];
            if($farmTypesByParent){
                foreach ($farmTypesByParent as $value){
                    $farmTypeId[]= $value->getId();
                }
            }

            if($breedType=='boiler'){
                $productsName[$breedParam] = $this->getDoctrine()->getRepository(Setting::class)->getProductTypeForBoilerChickByBreedName($farmTypeId);
            }elseif($breedType=='layer'){
                $productsName[$breedParam] = $this->getDoctrine()->getRepository(Setting::class)->getProductTypeForLayerChickByBreedName($farmTypeId);
            }else{
                $productsName[$breedParam] = $this->getDoctrine()->getRepository(Setting::class)->getProductTypeWithOutChickByBreedName($farmTypeId);
            }
        }

        return $this->render('@TerminalbdKpi/employeeboard/report/month-range/monthRangeWiseSummeryReport.html.twig', [
            'filterBy' => $filterBy,
            'form' => $filterForm->createView(),
            'data' => $data,
            'mode' => $mode,
            'months' => $months,
            'employees' => $employees,
            'chickTypes' => $chickTypes,
            'breedTypes' => $breedTypes,
            'feedTypes' => $feedTypes,
            'meatTypes' => $meatTypes,
            'eggTypes' => $eggTypes,
            'productsName' => $productsName,
            'breedNameArray' => $breedNameArray
        ]);
    }


    private function getMonthsInRange($startDate, $endDate) {
        $start = new \DateTime($startDate);
        $end = new \DateTime($endDate);
        $end->modify('first day of next month');

        $interval = new \DateInterval('P1M');
        $datePeriod = new \DatePeriod($start, $interval, $end);

        $months = [];
        foreach ($datePeriod as $date) {
            $months[$date->format('Y-m')] = $date->format('M Y');
        }

        return $months;
    }


    public function unique_array($my_array, $key) {
        $result = array();   // Initialize an empty array to store the unique values
        $i = 0;              // Initialize a counter
        $key_array = array(); // Initialize an array to keep track of encountered keys

        // Iterate through each element in the input array
        foreach($my_array as $val) {
            // Check if the key value is not already present in the key array
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];  // Store the key value in the key array
                $result[$i] = $val;           // Store the entire element in the result array
            }
            $i++;  // Increment the counter
        }

        // Return the array containing unique values based on the specified key
        return $result;
    }


}