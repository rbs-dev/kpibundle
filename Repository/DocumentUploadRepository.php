<?php
namespace Terminalbd\KpiBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class DocumentUploadRepository extends EntityRepository
{

}